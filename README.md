PostScript to generate background images that complement pixel orbiting for screens threatened by burn-in.

Optionally configure the script in pixel-orbiting-background.ps, then run

```bash
./generate.sh
```

to generate the image and save it in the output directory.