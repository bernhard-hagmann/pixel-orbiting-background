#!/bin/bash

postScriptFile=pixel-orbiting-background.ps
now=$(date +"%Y-%m-%d_%H-%M")
outputFile=output/bg-$now.png

gs -dNOPAUSE -sDEVICE=pngalpha -o $outputFile $postScriptFile
